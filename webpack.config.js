const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
module.exports = {
    devServer: {
        compress: true,
        port: 9876
    },
    entry: "./src/app/index.tsx",
    devtool: 'inline-source-map',
    target: "web",
    mode: "development",
    // mode: "production",
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "bundle.js",
    },
    resolve: {
        extensions: [".js", ".jsx", ".json", ".ts", ".tsx"],
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                loader: "awesome-typescript-loader",
            },
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader",
            },
            {
                test: /\.(scss|sass)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {loader: 'css-loader'},
                    {loader: 'sass-loader'},
                    // style-loader: not extract to style.min.css, mà sẽ import css vào head html luôn
                    //  'style-loader',
                    //  'css-loader',
                    //  'sass-loader',
                ]
            }
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "src", "index.html"),
        }),
        // sau khi build sẽ extract ra file css tương ứng
        new MiniCssExtractPlugin({
            filename: "style.min.css",
        }),
    ],
};
